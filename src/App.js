import { BrowserRouter as Router, Route, Routes, Link } from "react-router-dom";

import Post from "./components/posts/index";
import DetailPostComment from "./components/posts/detailPostComment";
import Photo from "./components/photos/index";
import './App.css';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Post />} />
        <Route exact path="/posts/:id/comments" element={<DetailPostComment />} />
        <Route path="/photos" element={<Photo />}>
          <Post />
        </Route>
      </Routes>
    </Router>
  );
}

export default App;
