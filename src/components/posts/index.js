import React, { useState, useEffect } from 'react'
import { DataGrid } from '@mui/x-data-grid';
import { Typography, Button } from '@mui/material';
import { useNavigate } from 'react-router-dom';

import './index.css'

export default function Post() {

    const [dataRow, setDataRow] = useState([]);
    const navigate = useNavigate();

    const handleClickComment = (element) => {
        navigate(`/posts/${element.id}/comments`)
    };

    const toPagePhoto = () => {
        navigate("/photos");
    }

    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/posts')
            .then(response => response.json())
            .then(json => setDataRow(json))
    }, []);

    const columns = [
        { field: 'userId', headerName: 'userID', width: 60 },
        { field: 'id', headerName: 'ID', width: 10 },
        { field: 'title', headerName: 'Title', width: 300 },
        { field: 'body', headerName: 'Body', width: 800, },
        {
            field: 'comment', headerName: 'Comment', width: 100, renderCell: (params) => {

                return (
                    <Button
                        key={params.id}
                        onClick={() => handleClickComment(params.row)}
                        variant="contained"
                    >
                        COMMENT
                    </Button>

                );
            }
        },
    ]

    return (
        <>
            <Typography className='title-post'>LIST POSTS</Typography>
            <Button onClick={toPagePhoto} className="to-page">Back page post</Button>
            <DataGrid style={{ width: '100%' }}
                rows={dataRow}
                columns={columns}
                initialState={{
                    pagination: {
                        paginationModel: {
                            pageSize: 10,
                            page: 0
                        },
                    },
                }}
                pageSizeOptions={[10, 20, 50]}
            >
            </DataGrid>
        </>
    )

}
