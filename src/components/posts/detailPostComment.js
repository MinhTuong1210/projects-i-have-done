import React, { useState, useEffect } from 'react'
import { useLocation } from 'react-router-dom';
import { DataGrid } from '@mui/x-data-grid';
import { Typography, Button, DialogTitle, FormControl, DialogActions, Dialog, TextField, DialogContent } from '@mui/material';
import { useNavigate } from 'react-router-dom';

import './index.css';

function DetailPostComment() {
    const [detailPost, setDetailPost] = useState(localStorage?.getItem('data') ? JSON.parse(localStorage.getItem('data')) : []);
    const location = useLocation();
    const id = parseInt(location.pathname.split('/')[2]);
    const [open, setOpen] = useState(false);
    const [name, setName] = useState('')
    const [contentComment, setContentComment] = useState('')
    const [email, setEmail] = useState('');
    const navigate = useNavigate();

    useEffect(() => {
        fetch(`https://jsonplaceholder.typicode.com/posts/${id}/comments`)
            .then(response => response.json())
            .then(json => setDetailPost(json))


    }, []);

    const columns = [
        { field: 'id', headerName: 'ID', width: 50 },
        { field: 'name', headerName: 'Name', width: 300 },
        { field: 'email', headerName: 'Email', width: 200 },
        { field: 'body', headerName: 'Body', width: 800, },

    ]

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleComment = () => {
        localStorage.setItem("data", JSON.stringify(detailPost).toString())
        let dataDetailPost = Object.values(JSON.parse(localStorage.getItem('data')));
        let idComment = detailPost[detailPost.length - 1].id + 1;
        let data = { postId: id, name: name, email: email, body: contentComment, id: idComment };
        dataDetailPost.push(data);
        setDetailPost(dataDetailPost)
        localStorage.removeItem("data");
        localStorage.setItem("data", JSON.stringify(dataDetailPost).toString());
        setOpen(false);
    }

    const backPagePost = () => {
        navigate("/");
    }

    return (
        <>
            <Typography className='title-post'>DETAIL POST</Typography>
            <Button onClick={backPagePost} className="to-page">Back page post</Button>
            <br/>
            <Button onClick={handleClickOpen}>Add comment</Button>
            <DataGrid style={{ width: '100%' }}
                rows={detailPost}
                columns={columns}
                initialState={{
                    pagination: {
                        paginationModel: {
                            pageSize: 5,
                            page: 0
                        },
                    },
                }}
                pageSizeOptions={[5, 10]}
            >
            </DataGrid>
            <Dialog open={open} onClose={handleClose}>
                <DialogTitle>Add comment</DialogTitle>
                <DialogContent style={{ width: '900px' }}>
                    <FormControl>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="email"
                            label="Email Address"
                            type="email"
                            fullWidth
                            variant="standard"
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                        />
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Name"
                            type="text"
                            fullWidth
                            variant="standard"
                            value={name}
                            onChange={e => setName(e.target.value)}
                        />
                        <TextField
                            autoFocus
                            margin="dense"
                            id="comment"
                            label="Content Comment"
                            type="text"
                            fullWidth
                            variant="standard"
                            value={contentComment}
                            onChange={e => setContentComment(e.target.value)}
                        />
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button type="submit" onClick={() => handleComment()}>Comment</Button>
                </DialogActions>
            </Dialog>
        </>
    )
}

export default DetailPostComment