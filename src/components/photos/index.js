/* eslint-disable array-callback-return */
import React, { useState, useEffect } from 'react'
import { Typography, ImageList, ImageListItem, Input, Grid, Button } from '@mui/material';
import { useNavigate } from 'react-router-dom';

import './index.css';

export default function Photo() {

    const [data, setData] = useState([]);
    const [color, setColor] = useState();
    const navigate = useNavigate();


    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/photos')
            .then(response => response.json())
            .then(json => setData(json))
    }, []);

    function srcset(image, size, rows = 1, cols = 1) {
        return {
            src: `${image}?w=${size * cols}&h=${size * rows}&fit=crop&auto=format`,
            srcSet: `${image}?w=${size * cols}&h=${size * rows
                }&fit=crop&auto=format&dpr=2 2x`,
        };
    }

    const handleSearch = () => {
        console.log(data[0])
        let dataSearch = data.filter(item => {
            if (item.url.includes(color)) {
                return true;
            } else return false;
        });
        setData(dataSearch)
    };

    const backPagePost = () => {
        navigate("/");
    }

    return (
        <>
            <Typography className='title-post'>LIST PHOTOS</Typography>
            <Typography className="text-search">Search color</Typography>
            <Grid className="grid-search">
                <Input className="input-search" id="color" name="color" onChange={e => setColor(e.target.value)} />
                <Button className="btn-search" onClick={handleSearch}>Search</Button>
            </Grid>
            <Button onClick={backPagePost} className="to-page">Back page post</Button>

            <ImageList
                sx={{ width: '100%', height: '100%' }}
                variant="quilted"
                cols={5}
                rowHeight={121}
            >
                {data.map((item) => (
                    <ImageListItem key={item.id} cols={1} rows={1}>
                        <img
                            // eslint-disable-next-line no-undef
                            {...srcset(item.url, 121, item.rows, item.cols)}
                            alt={item.title}
                            loading="lazy"
                        />
                    </ImageListItem>
                ))}
            </ImageList>
        </>
    )

}
